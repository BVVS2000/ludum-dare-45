﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomUpgrader : MonoBehaviour
{
    private EmployerClickUpgrade employerClickUpgrade;
    private EmployerEffinencyUpgrade employerEffinencyUpgrade;

    private void Start() 
    {
        employerClickUpgrade = GetComponent<EmployerClickUpgrade>();
        employerEffinencyUpgrade = GetComponent<EmployerEffinencyUpgrade>();
    }

    public void LevelUpEffinency()
    {
        if(employerEffinencyUpgrade.LevelUp())
        {
            //succes 
        }else
        {
            //failed
        }
    }

    public void LevelUpClick()
    {
        if(employerClickUpgrade.LevelUp())
        {
            //succes 
        }else
        {
            //failed
        }
    }
}
