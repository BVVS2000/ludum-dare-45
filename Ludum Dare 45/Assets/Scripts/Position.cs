﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Position : Interactable
{
    private Employerator employerator;
    private ComputerCountUpgrade computerCountUpgrade;

    [SerializeField] private GameObject computer;
    [SerializeField] private GameObject employer;
    [SerializeField] private Text popUpText;

    private void Start() 
    {
        employerator = FindObjectOfType<Employerator>();
        computerCountUpgrade = FindObjectOfType<ComputerCountUpgrade>();
    }

    private void Update() 
    {
        if(!transform.Find("Computer(Clone)"))
        {
            popUpText.text = "Buy computer (" + computerCountUpgrade.lvlUpCost + " HS)";
        }
    }

    public override bool Interact()
    {
        if(transform.Find("Computer(Clone)") && !transform.Find("employer(Clone)"))
        {
            if(employerator.LevelUp())
            {
                Instantiate(employer, transform.position, Quaternion.identity, transform);
                popUpText.text = "";
                return true;
            }
        }
        else if(!transform.Find("Computer(Clone)"))
        {
            if(computerCountUpgrade.LevelUp())
            {
                transform.GetComponentInChildren<SpriteRenderer>().enabled = false;
                Instantiate(computer, transform.position, Quaternion.identity, transform);
                popUpText.text = "Hire employer (" + employerator.lvlUpCost + " HS)";
                return true;
            }
        }

        return false;
    }

    public override void ShowUI()
    {
        popUpText.gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        popUpText.gameObject.SetActive(false);
    }
}