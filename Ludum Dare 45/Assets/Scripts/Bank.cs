﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bank : MonoBehaviour
{
    public int happySouls { get; private set; }
    public int maxSouls { get; private set; }
    private int baseSouls = 200;
    private int tempbankMaxCapacity = 0;
    
    private BankMaxCapacityUpgrade bankMaxCapacityUpgrade;
    private PlayerClickUpgrade playerClickUpgrade;
    private ResetartGame resetartGame;

    [SerializeField] private Text souls;

    private void Start() 
    {
        playerClickUpgrade = FindObjectOfType<PlayerClickUpgrade>();
        bankMaxCapacityUpgrade = FindObjectOfType<BankMaxCapacityUpgrade>();
        resetartGame = FindObjectOfType<ResetartGame>();
        happySouls = 0;
        UpdateSouls();
    }

    public void Add(int ammount)
    {
        ammount*=resetartGame.restartsCount + 1;
        if(happySouls+ammount>maxSouls)
        {
            happySouls = maxSouls;
        }
        else if(happySouls<maxSouls)
        {
            happySouls += ammount;
        }

        UpdateSouls();
    }

    public bool Spend(int ammount)
    {
        if(ammount > happySouls)
        {
            return false;
        }

        happySouls -= ammount;
        UpdateSouls();

        return true;
    }

    private void Update()
    {
        if(tempbankMaxCapacity != bankMaxCapacityUpgrade.value)
        {
            UpdateSouls();
            tempbankMaxCapacity++;
        }
    }

    private void UpdateSouls()
    {
        maxSouls = baseSouls * (int)bankMaxCapacityUpgrade.value;
        souls.text = happySouls.ToString() + "/" + maxSouls.ToString() + " HS";
    }
}
