﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RoomBuilder : MonoBehaviour
{
    private RoomCountUpgrade roomCountUpgrade;

    private int tempRoomCount = 0;
    [SerializeField] private float roomOffset;
    [SerializeField] private GameObject room;

    private void Start() 
    {
        roomCountUpgrade = FindObjectOfType<RoomCountUpgrade>();
    }

    private void Update() 
    {
        if(tempRoomCount != roomCountUpgrade.value)
        {
            Instantiate(room, Vector3.left * (tempRoomCount++ * roomOffset), Quaternion.identity);
        }
    }
}
