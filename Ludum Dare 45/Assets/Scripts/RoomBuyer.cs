﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomBuyer : Interactable
{
    private RoomCountUpgrade roomCountUpgrade;
    [SerializeField] Canvas canvas;
    [SerializeField] Text text;

    private void Start() 
    {
        roomCountUpgrade = FindObjectOfType<RoomCountUpgrade>();
    }

    public void Buy()
    {
        if(roomCountUpgrade.LevelUp())
        {
            Destroy(gameObject);
        }
        else
        {
            
        }
    }

    public override bool Interact()
    {
        Buy();
        return true;
    }

    public override void ShowUI()
    {
        text.text = "BuyRoom\n" + roomCountUpgrade.lvlUpCost + "HS";
        canvas.gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        canvas.gameObject.SetActive(false);
    }
}
