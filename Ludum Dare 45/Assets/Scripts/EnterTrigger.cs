﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class EnterTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) 
    {
        Debug.Log("1");
        if(other.CompareTag("Player"))
        {
            Debug.Log("2");
            if(other.transform.position.x < transform.position.x)
            {
                Debug.Log("3");
                FindObjectOfType<CameraMovement>().ChangeRoom(Direction.Forward);
                float distance = Mathf.Abs(transform.position.x - other.transform.position.x) * 3;
                other.transform.position += Vector3.right * distance;
            }
            else if(other.transform.position.x > transform.position.x)
            {
                Debug.Log("4");
                FindObjectOfType<CameraMovement>().ChangeRoom(Direction.Backward);
                float distance = Mathf.Abs(transform.position.x - other.transform.position.x) * 3;
                other.transform.position += Vector3.left * distance;
            }
        }
    }
}
