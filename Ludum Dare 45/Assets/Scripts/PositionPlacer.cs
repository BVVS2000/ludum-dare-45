﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionPlacer : MonoBehaviour
{
    [SerializeField] GameObject positionPrefab;
    [SerializeField] private float offset;
    [SerializeField] private Vector3 startPos;

    public int computerCount { get => FindObjectsOfType<Position>().Length;}

    private void Start() 
    {
        Place();
    }

    public void Place()
    {
        Instantiate(positionPrefab, startPos + Vector3.left * (computerCount+1) * offset, Quaternion.identity);
    }
}
