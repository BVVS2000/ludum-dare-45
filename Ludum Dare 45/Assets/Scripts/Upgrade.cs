﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Upgrade : MonoBehaviour
{
    protected Bank bank;

    public int Level  { get; protected set; }
    public float value { get; protected set; }
    public int lvlUpCost { get { return (int)((float)baseLevelUpCost * Mathf.Pow(1.15f, Level)); } }
    [SerializeField] protected float valueIncreseByLevel = 1;
    [SerializeField] protected int baseLevelUpCost;

    public string desciption = "";    

    public abstract bool LevelUp();
}
