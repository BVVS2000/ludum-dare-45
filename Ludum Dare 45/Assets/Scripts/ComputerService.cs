﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class ComputerService : Interactable
{
    public int computersBroken { get => FindObjectsOfType<ComputerCondition>().Where(t => t.broken && !t.wasSentHelp).ToList().Count; }
    public ComputerCondition brokenComputer { get => FindObjectsOfType<ComputerCondition>().Where(t => t.broken && !t.wasSentHelp).FirstOrDefault(); }

    [SerializeField] private int serivceCost;
    [SerializeField] private Text computersBrokenTxt;
    [SerializeField] Text popUp;
    public AudioSource tel;

    private Bank bank;

    private void Start() 
    {
        bank = FindObjectOfType<Bank>();
    }

    private void Update() 
    {
        computersBrokenTxt.text = "Computers broken: " + computersBroken;
    }
    
    public override bool Interact()
    {
        if(computersBroken != 0 && bank.Spend(serivceCost))
        {
            brokenComputer.Repair();
            tel.Play();
            return true;
        }

        return false;
    }

    public override void ShowUI()
    {
        popUp.text = "Call serviceman (" + serivceCost + "HS)";
        popUp.gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        popUp.gameObject.SetActive(false); 
    }
}
