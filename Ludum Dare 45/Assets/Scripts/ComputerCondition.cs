﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerCondition : MonoBehaviour
{
    [SerializeField] private float minDestroyTime = 90f;
    [SerializeField] private float maxDestroyTime = 150f;
    [SerializeField] private float repairTime = 10f;
    [SerializeField] private GameObject serviceman;

    public bool broken {get; private set;}
    public bool wasSentHelp { get; private set; }
    public AudioSource klawa;
    public AudioSource kurwa;

    public AudioSource jebut;
    AudioSource komp;
    private void Start() 
    {
        StartCountDown();
        klawa.Play();
        komp = transform.parent.Find("Computer(Clone)").GetComponentInChildren<AudioSource>();
    }

    private void StartCountDown()
    {
        Invoke("Break", Random.Range(minDestroyTime, maxDestroyTime));
    }

    public void Break()
    {
        broken=true;
        klawa.Pause();
        kurwa.Play();
        komp.Pause();
        jebut.Play();
    }

    public void Repair()
    {
        StartCoroutine("StartRepairing");
    }

    private IEnumerator StartRepairing()
    {
        wasSentHelp = true;
        GameObject obj = Instantiate(serviceman, transform.position + serviceman.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(repairTime);
        
        komp.Pause();
        klawa.Play();
        
        Destroy(obj);
        wasSentHelp = false;
        broken = false;
        StartCountDown();

    }
}
