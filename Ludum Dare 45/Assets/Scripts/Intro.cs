﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public SpriteRenderer tx1;
    public SpriteRenderer tx2;
    public SpriteRenderer tx3;
    public SpriteRenderer tx4;
    public SpriteRenderer tx5;
    public bool e=false;
    public AudioSource dub;

    private Animator animacja;
    void Start()
    {
        tx1.enabled=false;
        tx2.enabled=false;
        tx3.enabled=false;
        tx4.enabled=false;
        tx5.enabled=false;
        animacja = transform.GetComponentInChildren<Animator>();
        
        StartCoroutine(Example());
    }

    void Update()
    {
        animacja.SetBool("e", e);
        if(Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(Skip());
            e=true;
        } 
    }
    IEnumerator Example()
    {
        yield return new WaitForSeconds(5);
        dub.Play();
        tx1.enabled=true;
        yield return new WaitForSeconds(12);
        tx1.enabled=false;
        tx2.enabled=true;
        yield return new WaitForSeconds(11);
        tx2.enabled=false;
        tx3.enabled=true;
        yield return new WaitForSeconds(10);
        tx3.enabled=false;
        tx4.enabled=true;
        yield return new WaitForSeconds(13);
        tx4.enabled=false;
        tx5.enabled=true;
        yield return new WaitForSeconds(10);
        tx5.enabled=false;
        SceneManager.LoadScene("Game", LoadSceneMode.Single);

    }
    IEnumerator Skip()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
        
    }
}
