﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour
{
    private PlayerClickUpgrade playerClickUpgrade;
    private BankMaxCapacityUpgrade bankMaxCapacityUpgrade;

    [SerializeField] private Text playerCost;
    [SerializeField] private Text bankCost;

    private void Awake() 
    {
        playerClickUpgrade = GetComponent<PlayerClickUpgrade>();
        bankMaxCapacityUpgrade = GetComponent<BankMaxCapacityUpgrade>();
    }

    private void Update() 
    {
        playerCost.text = playerClickUpgrade.lvlUpCost + "HS";
        bankCost.text = bankMaxCapacityUpgrade.lvlUpCost + "HS";
    }

    public void UpgradePlayer()
    {
        playerClickUpgrade.LevelUp();
    }

    public void UpgradeBank()
    {
        bankMaxCapacityUpgrade.LevelUp();
    }
}
