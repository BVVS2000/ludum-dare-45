﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ComputerImporter : MonoBehaviour
{    
    private ComputerCountUpgrade computerCountUpgrade;

    private int tempComputerCount = 0;
    [SerializeField] private GameObject computer;
    [SerializeField] private float offset;
    [SerializeField] private Vector3 startPos;

    private void Start() 
    {
        computerCountUpgrade = FindObjectOfType<ComputerCountUpgrade>();    
    }

    private void LateUpdate() 
    {
        GameObject pos = GameObject.FindGameObjectsWithTag("Position").Where(t => t.transform.childCount==1).FirstOrDefault();
        if(tempComputerCount != computerCountUpgrade.value)
        {
            tempComputerCount++;
            Instantiate(computer, pos.transform.position, Quaternion.identity, pos.transform);
        }
    }
}
