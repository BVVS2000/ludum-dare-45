﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ATM : MonoBehaviour
{
    private Bank bank;
    private ResetartGame resetartGame;

    public int soulsPerSecond 
    { 
        get
        {
            List<Clicker> clickers = new List<Clicker>(FindObjectsOfType<Clicker>());
            int sum = 0;

            foreach (Clicker c in clickers)
            {
                sum += c.happySoulsPerSecond * c.clicksPerSecond;
            }

            return sum;
        } 
    }

    [SerializeField] private Text soulsPerSecondTxt;

    private void Start() 
    {
        resetartGame = FindObjectOfType<ResetartGame>();
        bank = FindObjectOfType<Bank>();
        Invoke("CashIn", 1f);
    }

    private void Update() 
    {
        soulsPerSecondTxt.text = "HS/sec: " + soulsPerSecond  * (resetartGame.restartsCount+1);
    }

    public void CashIn()
    {
        bank.Add(soulsPerSecond);
        Invoke("CashIn", 1f);
    }
}
