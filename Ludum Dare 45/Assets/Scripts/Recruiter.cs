﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Recruiter : MonoBehaviour
{
    private Employerator employerator;
    private int tempEmployerCount = 0;

    [SerializeField] private GameObject employer;

    private void Start() 
    {
        employerator = FindObjectOfType<Employerator>();
    }

    private void Update() 
    {
        GameObject computer = GameObject.FindGameObjectsWithTag("Computer").Where(t => t.transform.childCount==2).FirstOrDefault();

        if(tempEmployerCount != employerator.value)
        {
            tempEmployerCount++;
            Instantiate(employer, computer.transform.position, Quaternion.identity, computer.transform);
        }
    }
}
