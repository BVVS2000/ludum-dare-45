﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction { Forward, Backward }

public class CameraMovement : MonoBehaviour
{
    private int roomId = 0;
    private Vector3 roomPos { get { return new Vector3(startPos.x + roomOffset * roomId, startPos.y, startPos.z);} }
    private Vector3 startPos;
    
    [SerializeField] private float roomOffset = 10f;
    [SerializeField] private float speed = 1f;

    private void Start() 
    {
        startPos = transform.position;
    }

    public void ChangeRoom(Direction direction = Direction.Forward)
    {
        if(direction == Direction.Forward)
        {
            roomId++;
        }
        else
        {
            roomId--;            
        }
    }
    private void FixedUpdate() 
    {
        if(transform.position != roomPos)
        {
            float x = (transform.position.x-roomPos.x)>0?-1:1;
            transform.Translate(Vector2.right * x * speed * Time.deltaTime);
            FindObjectOfType<CharacterMovement>().enabled = false;

            if(Vector2.Distance(transform.position, roomPos) <= 0.01)
            {
                transform.position = roomPos;
                FindObjectOfType<CharacterMovement>().enabled = true;
            }
        }
    }
}
