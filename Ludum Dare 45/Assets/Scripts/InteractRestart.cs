﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractRestart : Interactable
{
    [SerializeField] Text popUp;
    private GameObject canvas;
    public AudioSource restartrestart;
    private void Start() 
    {
        canvas = GameObject.FindGameObjectWithTag("Restart");
        canvas.SetActive(false);
    }

    private void Update() 
    {
        if(canvas && canvas.activeInHierarchy)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                canvas.SetActive(false);
            }
        }
    }

    public override bool Interact()
    {
        canvas.SetActive(true);
        restartrestart.Play();
        ResetartGame rg = FindObjectOfType<ResetartGame>();
        canvas.transform.GetComponentInChildren<Text>().text = "Are you not handling corporation anymore? If you will earn enough money(" + rg.resetartCost * (rg.restartsCount +1) + "HS) you can restart the game and gain income bonus";
        return true;
    }

    public override void ShowUI()
    {
        popUp.gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        popUp.gameObject.SetActive(false); 
    }

    public void Restart()
    {
        if(FindObjectOfType<Bank>().Spend(FindObjectOfType<ResetartGame>().resetartCost))
        {
            FindObjectOfType<ResetartGame>().Restart();
        }
    }
}
