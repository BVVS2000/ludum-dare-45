﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : Interactable
{
    [SerializeField] private GameObject canvas;
    [SerializeField] private Text popUp;
    public AudioSource book;

    private void Start() 
    {
        canvas = GameObject.FindGameObjectWithTag("Upgrade");
        canvas.SetActive(false);
    }

    private void Update() 
    {
        if(canvas && canvas.activeInHierarchy)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                canvas.SetActive(false);
            }
        }
    }

    public override bool Interact()
    {
        canvas.SetActive(true);
        book.Play();
        return true;
    }

    public override void ShowUI()
    {
        popUp.gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        popUp.gameObject.SetActive(false); 
    }
}
