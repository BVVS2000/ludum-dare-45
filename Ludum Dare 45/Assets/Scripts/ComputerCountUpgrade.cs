﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ComputerCountUpgrade : Upgrade
{
    private RoomCountUpgrade roomCountUpgrade;

    private void Start() 
    {
        bank = FindObjectOfType<Bank>();
        Level = 0;
        value = Level * valueIncreseByLevel;

        roomCountUpgrade = FindObjectOfType<RoomCountUpgrade>();
    }

    public override bool LevelUp()
    {
        bool found = false;

        if(GameObject.FindGameObjectsWithTag("Position").Where(t => t.transform.childCount==2).FirstOrDefault() != null)
        {
            found = true;
        }

        if(found && bank.Spend(lvlUpCost))
        {
            value = ++Level * valueIncreseByLevel;
            return true;
        }
        else
        {
            return false;
        }
    }
}
