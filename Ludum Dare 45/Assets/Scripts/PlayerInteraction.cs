﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField] private float distance;

    private Interactable interactable;

    private void Update() 
    {
        RaycastHit [] hits = new RaycastHit[4];
        bool [] hitted = new bool[4];
        bool found = false;
        float minDistance = Mathf.Infinity;
        Interactable temp = interactable;

        hitted[0] = Physics.Raycast(transform.position, Vector3.forward, out hits[0], distance, LayerMask.GetMask("Interactable"));
        hitted[1] = Physics.Raycast(transform.position, Vector3.back, out hits[1], distance, LayerMask.GetMask("Interactable"));
        hitted[2] = Physics.Raycast(transform.position, Vector3.left, out hits[2], distance, LayerMask.GetMask("Interactable"));
        hitted[3] = Physics.Raycast(transform.position, Vector3.right, out hits[3], distance, LayerMask.GetMask("Interactable"));

        if(interactable)
        {
            interactable.HideUI();
        }

        for(int i=0; i<4; i++)
        {
            if(hitted[i])
            {
                if(hits[i].distance < minDistance)
                {
                    found = true;
                    minDistance = hits[i].distance;
                    interactable = hits[i].transform.GetComponent<Interactable>();
                }
            }
        }

        if(!found)
        {
            if(interactable)
            {
                interactable.HideUI();
            }
            interactable = null;
        }

        if(interactable)
        {
            interactable.ShowUI();
            if(Input.GetKeyDown(KeyCode.E))
            {
                interactable.Interact();
            }
        }
    }
}
