﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Employerator : Upgrade
{
    private RoomCountUpgrade roomCountUpgrade;

    private void Start() 
    {
        roomCountUpgrade = FindObjectOfType<RoomCountUpgrade>();
        
        bank = FindObjectOfType<Bank>();
        Level = 0;
        value = Level * valueIncreseByLevel;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            LevelUp();
        }
    }

    public override bool LevelUp()
    {
        bool found = false;

        if(GameObject.FindGameObjectsWithTag("Computer").Where(t => t.transform.childCount==1).FirstOrDefault() != null)
        {
            found = true;
        }

        if(found && bank.Spend(lvlUpCost))
        {
            value = ++Level * valueIncreseByLevel;
            return true;
        }
        else
        {
            return false;
        }
    }
}
