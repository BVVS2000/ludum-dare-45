﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class Clicker : MonoBehaviour
{
    private Bank bank;
    private EmployerClickUpgrade employerClickUpgrade;
    private EmployerEffinencyUpgrade employerEffinencyUpgrade;

    [SerializeField] private Text happySoulsPerClickTxt;
    [SerializeField] private Text clicksPerSecondTxt;
    [SerializeField] private Text hspcp;
    [SerializeField] private Text cpsp;

    public int clicksPerSecond{ get => _clicksPerSecond * (int)employerEffinencyUpgrade.value; }
    public int happySoulsPerSecond { get => employersCount * (int)employerClickUpgrade.value; }
    public int employersCount 
    { 
        get => GameObject.FindGameObjectsWithTag("Employer").Where(t => t.transform.parent.parent == transform && !t.GetComponentInChildren<ComputerCondition>().broken).ToList().Count;
    }


    [SerializeField] private int _clicksPerSecond = 1;

    private float interval { get => 1 / _clicksPerSecond / employerEffinencyUpgrade.value; }
    private float toClick = 0.0f;

    private void Start() 
    {
        bank = FindObjectOfType<Bank>();
        employerClickUpgrade = GetComponent<EmployerClickUpgrade>();
        employerEffinencyUpgrade = GetComponent<EmployerEffinencyUpgrade>();
    }

    private void Update() 
    {
        /* 
        toClick -= Time.deltaTime;
        
        if(toClick<=0.0f)
        {
            toClick = interval;
            bank.Add((int)employerClickUpgrade.value * employersCount);
        }
        */
        happySoulsPerClickTxt.text = "Happy Souls per click: " + happySoulsPerSecond;
        clicksPerSecondTxt.text = "Clicks per second: " + clicksPerSecond;
        hspcp.text = employerClickUpgrade.lvlUpCost + "HS";
        cpsp.text = employerEffinencyUpgrade.lvlUpCost + "HS";
    }
}
