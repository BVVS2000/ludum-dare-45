﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerClick : Interactable
{
    private Bank bank;
    private PlayerClickUpgrade playerClickUpgrade;
    public AudioSource klik;
    
    [SerializeField] Text popUp;
    
    private void Start()
    {
        bank = FindObjectOfType<Bank>();
        playerClickUpgrade = FindObjectOfType<PlayerClickUpgrade>();
    }

    public override bool Interact()
    {
        bank.Add((int)playerClickUpgrade.value);
        klik.Play();
        return true;
        
    }

    public override void ShowUI()
    {
        popUp.gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        popUp.gameObject.SetActive(false); 
    }
}
