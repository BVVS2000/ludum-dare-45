﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientAmbient : MonoBehaviour
{
    public AudioSource A2;
    public AudioSource A3;
    public AudioSource A4;
    void Start()
    {
        StartCoroutine(Example());
    }

    IEnumerator Example()
    {
        for(;;)
        {
            A2.Play();
            yield return new WaitForSeconds(200);
            A3.Play();
            yield return new WaitForSeconds(230);
            A4.Play();
            yield return new WaitForSeconds(220);
        }
    }
}
