﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    
    private Rigidbody rb;
    private Animator animacja;
    public bool Klika = false;
    AudioSource steps;
    float inputX = 0;
    float inputY= 0;

    [SerializeField] private float stepOffset = 0.6f;
    private float asd = 0.0f;

    void Start() 
    {
        rb = GetComponent<Rigidbody>();
        animacja = transform.GetComponentInChildren<Animator>();
        steps = GetComponent<AudioSource>();
    }

    private void Update() 
    {
    
        if(inputX!=0 || inputY!=0)
        {
            if(asd>0.0f)
            {
                asd -= Time.deltaTime;
            }
            else
            {
                steps.Play();
                asd = stepOffset;
            }
            
            Klika=true;
            
        }
        else if (inputX==0 || inputY==0)
        {
            Klika=false;
            steps.Stop();
            asd = 0;
        }
    }

    void FixedUpdate()
    {
        inputX = Input.GetAxisRaw("Horizontal");
        inputY= Input.GetAxisRaw("Vertical");
        animacja.SetBool("Kliko", Klika);

        if(inputX>0)
        {
            transform.localScale = new Vector3(0.008f, 0.008f, 0.008f);
        }
        else if (inputX<0)
        {
            transform.localScale = new Vector3(-0.008f, 0.008f, 0.008f);
        }

        //rb.MovePosition(transform.position + new Vector3(inputX, 0f, inputY*2.5f) * speed * Time.deltaTime);
        rb.velocity = new Vector3(inputX, 0f, inputY*2.5f) * speed;
    }
}
