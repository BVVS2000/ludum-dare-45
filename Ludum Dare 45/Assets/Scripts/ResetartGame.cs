﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResetartGame : MonoBehaviour
{
    public int restartsCount { get; private set; }

    public int resetartCost = 10000;

    private void Start() 
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Game");
        restartsCount++;
    }
}
