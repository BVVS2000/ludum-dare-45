﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmployerClickUpgrade : Upgrade
{
     private void Start() 
    {
        bank = FindObjectOfType<Bank>();
        Level = 1;
        value = Level * valueIncreseByLevel;
    }

    public override bool LevelUp()
    {
        if(bank.Spend(lvlUpCost))
        {
            value = ++Level * valueIncreseByLevel;
            return true;
        }
        else
        {
            return false;
        }
    }
}
