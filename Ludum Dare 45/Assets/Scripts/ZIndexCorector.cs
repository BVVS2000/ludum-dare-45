﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZIndexCorector : MonoBehaviour
{
    private GameObject player;

    private int minIndex = -9;
    private int maxIndex = 43;

    [SerializeField] private float distance = 5f;

    private SpriteRenderer sprite;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(Vector2.Distance(transform.position, player.transform.position) <= distance)
        {
            if(transform.position.z > player.transform.position.z)
            {
                sprite.sortingOrder = minIndex - 1;
            }
            else
            {
                sprite.sortingOrder = maxIndex + 1;
            }
        }
    }
}
